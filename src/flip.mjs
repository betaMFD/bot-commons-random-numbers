import { SlashCommandBuilder, EmbedBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('flip')
  .setDescription('Flips a coin.');

export async function execute(interaction) {
  const result = Math.random() < 0.5 ? 'Heads' : 'Tails';

  const embed = new EmbedBuilder()
    .setColor(0x0099FF)
    .setTitle('Coin Flip')
    .setDescription(`You flipped a coin. It's ${result}!`);

  await interaction.reply({ embeds: [embed] });
}
