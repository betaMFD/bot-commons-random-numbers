import { SlashCommandBuilder, EmbedBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('random_number')
  .setDescription('Generates a random number between the specified range.')
  .addIntegerOption(option => option.setName('min').setDescription('Minimum number (default 1)').setRequired(false))
  .addIntegerOption(option => option.setName('max').setDescription('Maximum number (default 100)').setRequired(false));

export async function execute(interaction) {
  const min = interaction.options.getInteger('min') || 1;
  const max = interaction.options.getInteger('max') || 100;
  const result = Math.floor(Math.random() * (max - min + 1)) + min;

  const embed = new EmbedBuilder()
    .setColor(0x0099FF)
    .setTitle('Random Number Generated')
    .addFields(
      { name: 'Range', value: `From ${min} to ${max}`, inline: true },
      { name: 'Result', value: result.toString(), inline: true }
    );

  await interaction.reply({ embeds: [embed] });
}
